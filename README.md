* ZX Invaders is an implementation of Space Invaders written for the ZX Spectrum.
* The code was written both for educational purposes and in homage to the original
* game which opened the eyes of the world to what computers could do. 

* Version 1.0

The code has been tested and compiled on ZXSpin emulator only. My spectrum is still gathering dust in some garage cupboard.

Download the code and copy it into ZX Spins inbuild assembler.
Instructions on how to run included in the source.


NOte : This was actually forked pre this code - I was actually amazed and well thought it prudent to write a read me in case anyone else cares to read my flawed attempts at Z80 assembly. Good luck, most of this z80 code is from my head although there are familiar  routines from many magazines over the years that are recognisable for screen graphic manipulations. Credits to those 80's mags , many editors and subscribers who added to everyone's knowledge of Sir Clives Beer Mat!.
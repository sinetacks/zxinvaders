ORG 7ffdH


;--------------
;
; FINAL  - Full One Player Game
; USR 32765 to start
;
;--------------

; Jump Game_Initiaisation routine
BEGIN	JP G_INIT  ; JUMP TO THE GAME 

; Interupt Vectore table from $8000 to $9001
;initially set to $FF for IM2 					;call
INT_VECTORS 	
		ds 257 	

; variables limits 
; screen limits 
limitr equ 235
limitl equ 20
limitd equ 164 ; Alien on top of player
player_death equ 90 ; seconds player is dead after hit

; saucer edges foreground colour ( black = same as bg )
edge_color equ $00

; dummy return for placing calls for future changes.
RETURNZ	RET 

; BCD NUMBER STORE
; Scores get reset in the code.
BCDS1	DB 00H,02H,00H ; score player 1
BCDS2	DB 00H,40H,00H ; score player 2
BCDHS	DB 01H,10H,00H ; hi-score

; stored variables

gmode	DB 0 ; used for game loops
P1LIVES DB 0 ; number of lives left player 1
P2LIVES DB 0 ; number of lives left player 2
CREDITS DB 04H ; game credits store
TEMP    DW 0 ; THESE BYTES WILL GET CORRUPTED WITH CREDIT 
		  ; DECODE - USE AS A TEMP STORE.
PACTIVE DB 0 ; player active - for initialising player
		  ; after death or first fleet on new game

NUMALIENS db 0 ; store of the number of aliens 


;--------------
;GRAPHICS
; 8 bits per block


;--------------

; BITS FOR THE BUILDINGS ABOVE THE SHIP
BBASET	DB 07H,0FFH,0e0H
	DB 0FH,0FFH,0F0H
	DB 1FH,0FFH,0F8H
	DB 3FH,0FFH,0FcH
	DB 07FH,0FFH,0FeH
	DB 07FH,0FFH,0FeH
	DB 07FH,0FFH,0FeH
	DB 07FH,0FFH,0FeH
BBASEB	DB 07FH,0ffH,0FeH
	DB 07FH,0ffH,0FeH
	DB 07FH,0ffH,0FeH
	DB 07FH,0ffH,0FeH
	DB 07eH,001H,0FeH
	DB 07cH,000H,0feH
	DB 078H,000H,07eH
	DB 078H,000H,07eH

SAUCER  db 0,0
	db 07,0e0h
	db 1fh,0f8h
	db 3fh,0fch
	db 6ch,0b6h
	db 0ffh,0ffH
	db 39h, 9ch
	db 10h,08h

SAUCER_DESTROY 
	db 0,0
	db 03,0e0h
	db 1eh,078h
	db 1eh,0e8h
	db 3fh,07ch
	db 07ch,09eH
	db 09h, 90h
	db 10h,08h

SAUCER_300
	db 0,0
	db $1d,$dc
	db $05,$54
	db $1d,$54
	db $05,$54
	db $1d,$dc
	db 0h, 0h
	db 0h,0h


SAUCER_200
	db 0,0
	db $1d,$dc
	db $05,$54
	db $1d,$54
	db $11,$54
	db $1d,$dc
	db 0h, 0h
	db 0h,0h

SAUCER_150
	db 0,0
	db $05,$dc
	db $05,$14
	db $05,$d4
	db $04,$54
	db $05,$dc
	db 0h, 0h
	db 0h,0h


tALIEN0 dw 8001h
	dw 0c003h
	dw 0e007h
	dw 0b00dh
	dw 0f00fh
	dw 4002h
	dw 0a005h
	dw 500ah

tALIEN1
	dw 8001h
	dw 0c003h
	dw 0e007h
	dw 0b00dh
	dw 0f00fh
	dw 0a005h
	dW 1008h
	dW 2004h

mALIEN0 dw 1004h
	dw 2412h
	dw 0f417h
	dw 0dc1dh
	dw 0fc1fh
	dw 0f80fh
	dw 1004h
	dw 0808h 	

mALIEN1
	dw 1004h
	dw 2002h
	dw 0f007h
	dw 0d80dh
	dw 0fc1fh
	dw 0f417h
	dw 1414h
	dw 6003h 

bALIEN0 dw 0c003h
	dw 0f81fh
	dw 0fc3fh
	dw 09c39h
	dw 0fc3fh
	dw 6006h
	dw 0b00dh
	dw 0830h

bALIEN1
	dw 0c003h
	dw 0f81fh
	dw 0fc3fh
	dw 09c39h
	dw 0fc3fh
	dw 700eh
	dw 9833h
	dw 700ch


SHIP1	DB 00H , 80H
	DB 01H, 0c0H
	DB 01H, 0c0H
	DB 1FH, 0fcH
	DB 1FH, 0fcH
	DB 1FH, 0fcH
	DB 1FH, 0fcH
	DB 1fH, 0fcH

SHIP1_HIT
	DB 01H , 00H
	DB 00H, 010H
	DB 02H, 0c0H
	DB 12H, 00H
	DB 01H, 0c0H
	DB 46H, 080H
	DB 1fH, 0e4H
	DB 3fH, 0f5H

SHIP1_HITa
	DB 01H , 04H
	DB 82H, 019H
	DB 10H, 0c0H
	DB 02H, 02H
	DB 4bH, 031H
	DB 21H, 0c4H
	DB 1fH, 0f0H
	DB 37H, 0f2H


gpBULLET
	dw $8000
	dw $8000
	dw $8000
	dw $8000

gBlast  
	dw $0000
	dw $0000
	dw $c001
	dw $f003
	dw $e001
	dw $e003
	dw $f003
	dw $e001

gaBoom	dw $2424
	dw $4812
	dw $1008
	dw $0660
	dw $1008
	dw $4812
	dw $2424
	dw $0000

;squiggly shot
gaSHOT1
	dw $0001
	dw $8000
	dw $0001
	dw $8000

gaSHOT1a
	dw $8000
	dw $0001
	dw $8000
	dw $0001

;plunger shot
gaSHOT2
	dw $8000
	dw $8000
	dw $8000
	dw $c001

gaSHOT2a
	dw $8000
	dw $c001
	dw $8000
	dw $8000

;--------------
;BCD - BINARY CODED DECIMAL ROUTINES
;--------------

;DECODE
; ENTRY HL POINTS TO BCD NUMBER MEMORY
; FUNTION UNPACKS BCD CODED NUMBER INTO STRING.
DECODE	LD IX, DECODED
	LD B,3
LOOP	LD D,(HL)
	XOR A
	RLD
	ADD A,30H
	LD (IX+0),A
	XOR A
	RLD
	ADD A,30H
	LD (IX+1),A
	LD (HL),D
	INC HL
	INC IX
	INC IX
	DJNZ LOOP
	RET
;Decoded number temp store
DECODED DB "000000"

;BCDADD
;ENTRY HL TO THE BCD NUMBER MEMORY LOCATION LOW DIGIT
;FUNCTION INCREASES A BCD NUMBER BY 1

BCDADD	LD A,(HL)
DIGIT	ADD A,1
	DAA
	LD (HL),A
	DEC HL
	LD A, (HL)
HUNDRED	ADC A,0
	DAA
	LD (HL),A
	DEC HL
	LD A, (HL)
CREDIT	ADC A,0
	DAA
	LD (HL),A
	RET

;--------------
; ADDS2
; 
;--------------

ADDS2	LD A,1
        LD (DIGADD),A
	XOR A
        LD (CRDADD),A
	LD HL,BCDS2+2
	CALL BCDADD
	CALL UPDATES
	RET


;--------------
;SCREEN ADDRESS CALCULATION ROUTINES
;--------------


;CHRPOKE
; POKE A CHARACTER DIRECTLY ON THE SCREEN
; ENTRY A - CHAR CODE
; DE - TOP BYTE IN CHAR MAP FOR CHARACTER
; ADJUSTED TO ALLOW FOR CROSS CELL PRINTING.

CHRPOKE	PUSH DE
	LD DE, 3C00H
	LD H,0
	LD L,A
	ADD HL, HL
	ADD HL, HL
	ADD HL, HL
	ADD HL,DE
	POP DE
BITPOKE	PUSH DE
	LD B,8
CLOOP	LD A,(HL)
	LD (DE),A
	INC HL
	INC D
	LD A,D
	AND 07
	JR NZ,THEEND
	LD A,E
	ADD A,20H
	LD E,A
	CCF
	SBC A,A
	AND 0F8H
	ADD A,D
	LD D,A
THEEND	DJNZ CLOOP
	POP DE
	RET

;SADDR
; CALCULATE SCREEN ADDRESS FROM PRINT COORDS
; ENTER H = COL DOWN, L = ROW ALONG
; EXIT HL= ADDRESS FOR SCREEN MEMORY

SADDR	LD DE,4000H
	ADD HL,DE
	LD A,H
	AND 07
	RRCA
	RRCA
	RRCA
	OR L
	LD L,A
	LD A, 0F8H
	AND H
	LD H,A
	RET

;PLOT ROUTINE 
;ENTER H=y, L=x coords
;EXIT DE=ADDR
; C - Y COORD
PLOT	LD A,H
	LD C,A
	AND 07H
	ADD A,40H
	LD D,A
	LD A,H
	AND 0C0H
	RRCA
	RRCA	
	RRCA
	ADD A,D
	LD D,A
	LD A,H
	RLCA
	RLCA
	AND 0E0H
	LD E,A
	LD A,L
	RRCA
	RRCA
	RRCA
	AND 1FH
	ADD A,E
	LD E,A
	RET

;BITSEL SELECT THE BIT TO PLOT
; ENTER L=x coord
; EXIT A = BIT TO POKE TO SCREEN ADD
BITSEL	LD A,L
	AND 07H
	INC A
	LD B,A
	XOR A
	SCF
BITLOOP RRA
	DJNZ BITLOOP
	RET

;--------------
;NON ROM TEXT PRINT ROUTINES 
;--------------

;SCORES
; PUT SCORE A SCORE ON THE SCREEN 
; AT A POSITION USING MEMORY STORES.

SCORES	LD HL,(SCORE) ; THE BCD NUMBER
	CALL DECODE
	XOR A
	LD HL,(POSITION)
	CALL SADDR
	LD DE,DECODED
	EX DE,HL
	LD B,6
LOOP1	PUSH HL
	PUSH DE
	PUSH BC
	LD A, (HL)
	CALL CHRPOKE
	POP BC
	POP DE
	POP HL
	INC HL
	INC DE
	DJNZ LOOP1
	RET


;DCREDIT
; DISPLAY NUMBER OF CREDITS

DCREDIT LD HL,CREDITS ; THE BCD NUMBER
	CALL DECODE
	XOR A
	LD HL,171CH
	CALL SADDR
	LD DE,DECODED
	EX DE,HL
	LD B,2
LOOP1C	PUSH HL
	PUSH DE
	PUSH BC
	LD A, (HL)
	CALL CHRPOKE
	POP BC
	POP DE
	POP HL
	INC HL
	INC DE
	DJNZ LOOP1C
	RET	

;DP1LIVES 
; DISPLAY PLAYER 1 LIVES

DP1LIVES LD HL,P1LIVES ; THE BCD NUMBER
	CALL DECODE
	XOR A
	LD HL,1702H
	CALL SADDR
	LD DE,DECODED+1
	EX DE,HL
	LD B,1
P1DLOOP	PUSH HL
	PUSH DE
	PUSH BC
	LD A, (HL)
	CALL CHRPOKE
	POP BC
	POP DE
	POP HL
	INC HL
	INC DE
	DJNZ P1DLOOP
	RET	


;UPDATE ALL THE SCORES ON THE SCREEN.
; VARIABLES FOR STORING LOCATIONS

POSITION DW 0
SHOW_FLAG DB $07 ; bit 0  - PLAYER 1, 
			 ; bit 1  - PLAYER 2
			 ; bit 2  - HI-SCORE
SCORE   DW 0
;THIS NEEDS A FLAG TO IGNORE PLAYER 1, 2 DEPENDING ON WHO IS
;ACTUALLY PLAYING

; UPDATES
; UPDATE THE SCORES 
UPDATES	LD HL,BCDS1 ; ***STORE OF BCD SCORE ***
	LD (SCORE),HL
	LD HL, 0104H
	LD (POSITION),HL
	LD A,(SHOW_FLAG)
	BIT 0,A
	CALL NZ,SCORES
	LD HL,BCDHS
	LD (SCORE),HL
	LD HL,010DH
	LD (POSITION),HL
	LD A,(SHOW_FLAG)
	BIT 2,A
	CALL NZ,SCORES
	LD HL, BCDS2
	LD (SCORE),HL
	LD HL,0116H
	LD (POSITION),HL
	LD A,(SHOW_FLAG)
	BIT 1,A
	CALL NZ,SCORES
	RET

;STRING TO PRINT FOR SCORE TABLES
TEXT1	DB "SCORE<1> HI-SCORE SCORE<2>"
	DB 0
TEXT2	DB "CREDITS "
	DB 0

PLAYER1s1	DB "PLAYER ONE <1>"
	DB 0
BSCORES DB "      "
	DB 0


;STRINGZ
; POKE NULL TERMINATED STRING ONTO SCREEN AT LOCATION
; ENTER HL COLUMN ROW ; DE STRING LOCATION

STRINGZ	PUSH DE
	CALL SADDR
	POP DE
STRINGZ2	EX DE,HL
SLOOP	LD A, (HL)
	CP 0
	RET Z
	PUSH HL
	CALL CHRPOKE
	POP HL
	INC HL
	INC DE
	JR SLOOP

;PRINTS
; PRINTS THE SCORE ALONG THE TOP LINE AND 
; PRINTS THE SCORES UNDERNEATH AND
; MAKES THE ATTRIBUTES THE RIGHT COLOURS.
PRINTS	CALL BLACKOUT ; 
	LD HL,0003H
	LD DE,TEXT1
	CALL STRINGZ
	LD HL,1714H
	LD DE,TEXT2
	CALL STRINGZ
	CALL UPDATES
	CALL DCREDIT
	LD HL,5800H
	LD DE,5801H
	LD BC,544
	LD (HL),71 ; BRIGHT WHITE
	LDIR
	LD BC,212
	LD (HL),68; BRIGHT GREEN FOR THE BUILDINGS
	LDIR
	LD BC,11  ;CREDITS WHITE BIT BOTTOM LINE
	LD (HL),71
	LDIR
	LD HL,22592
	LD DE, 22593
	LD BC, 31
	LD (HL),42H; BRIGHT RED FOR THE SHIP
	LDIR
	RET


;--------------
;SCREEN SET UP ROUTINES
;--------------


;BLACKOUT
; COMPLETELEY BLACKS THE SPECTRUM SCREEN
; NO TEXT WILL SHOW - NOT PERMANENT CHANGE
; 
BLACKOUT	XOR A
		OUT (0FEH),A ; BORDER 0
		LD HL,4000H
		LD DE,4001H
		LD BC,1AFFH
		LD (HL),0
		LDIR		  ; PAPER and INK 0
		RET

;EDGES
;Colours the edges of the red attributes section black ink on
;black paper so sprite isnt seen and scrolls onto screen
;smoothly. 

EDGES	ld hl,$5840
	ld d,h
	ld e,l
	inc de
	ld bc,1
	ld (hl),edge_color  ; needs to be 00
	ldir
	ld bc,29
	add hl,bc
	ld d,h
	ld e,l
	inc de
	ld bc,1
	ld (hl),edge_color; needs to be 00
	ldir
	ret


;BOTL
;line AT BOTTOM of the play area.
BOTL	LD L,16
	ld H,183
	CALL PLOT
	PUSH DE
	POP HL
	LD (HL),$FF
	INC DE
	LD BC,27
	LDIR
	RET


;--------------
;KEYBOARD ROUTINES 
;--------------

;DEMOKB for COINS+PLAY
;GAMKKB for player action
; global routine
; q set keyflag bit 7 and carry flag
; 8 sets bit 2 in keyflag
; 5 sets bit 1 in keyflag
; 1 sets bit 0 in keyflag


KEYFLAG	DB 00
DEMOKB	XOR A ; makes the player press bits 0.	
	CCF
	LD HL,KEYFLAG
	LD (HL),A
	LD BC,63486 ; KRY ROW 1 TO 5
	IN A,(C)
	BIT 4,A
	JR NZ,NOT5
	SET 1,(HL)
	JR CHK1
NOT5	XOR A
	LD (LATCH),A
CHK1	; LD BC,63486 ; KRY ROW 1 TO 5
	IN A,(C)
	BIT 0,A
	JR NZ,NOT1
	SET 0,(HL)
NOT1    LD BC,64510 ; KEY ROW Q TO T
	IN A,(C)
	BIT 0,A
	JR NZ,NOTQ
	SET 7,(HL)
NOTQ	RET


; Bit 7 is quit
; Bit 6 is player been hit.
gameKB	LD HL,KEYFLAG
	LD A,(HL)
	AND $C0 ; two msb bits survive the loop
	LD (HL),A
	bit 6,(hl)
	ret nz ; ignore all movement code when player hit
	SCF
	CCF
	LD BC,63486 ; KRY ROW 1 TO 5
	IN A,(C)
	BIT 4,A
	JR NZ,NOTg5
	SET 1,(HL)
NOTg5	LD BC,61438 ; KRY ROW 6 TO 0
	IN A,(C)
	BIT 2,A
	JR NZ,NOTG8
	SET 0,(HL)
NOTG8	BIT 0,A
	JR NZ,NOTG0
	SET 2,(Hl)
NOTG0    LD BC,64510 ; KEY ROW Q TO T
	IN A,(C)
	BIT 0,A
	JR NZ,NOTGQ
	SET 7,(HL)
NOTGQ	RET


;UPCREDIT
; increases credits until it is 99
;LATCH 
; keyboard latch so credit key cannot be held to stack
; credits.

;address variable for where digits and credits are
DIGADD EQU DIGIT+1
CRDADD EQU CREDIT+1

LATCH DB 0
UPCREDIT LD HL,LATCH
	 BIT 0,(HL)
	 RET NZ ; COIN NOT ACCEPTED YET
	 LD A,(CREDITS)
	 CP 099H
	 RET Z ; MAXIMUM CREDITS
	 XOR A	
         LD (DIGADD),A
	 INC A
	 LD (CRDADD),A
	 LD HL,CREDITS+2
	 CALL BCDADD
	 CALL DCREDIT
	 LD HL,LATCH
	 SET 0,(HL)
	 RET


;--------------
;STORED VARIABLES INITIALISE
;--------------


;INITIALISE SETS ALL SCORES TO ZERO
; SETS CREDITS TO ZERO
;SETS GAME MODE TO ZERO
INIT	LD HL,BCDS1
	ld b,13
zerol	LD (HL),0
	INC HL
	djnz zerol
	ld a,$07
	ld (show_flag),a
	; some missed will check them all
	ld ix,SPR_SAUCER
	ld (ix+0),$00
	ld ix,SPR_SAUCER_DESTROY
	ld (ix+0),$00
	ret


;--------------
;GAME MODE JUMP TABLES
;--------------

JUMPTAB
	DW GMODE0 ; intro screens
	DW GMODE1 ; initialise screen
	DW GMODE2 ; initialise scores
	DW GMODE3 ; player 1 play
	dw GMODE4 ; player cleared aliens
	dw GMODE5 ; reset aliens after clear
	dw GMODE6 ; invaded
	dw GMODE7 ; out of lives
	DW 0000 ; DEFAULT SKIP

GMODE_CALL JP (HL)


;--------------
;GAME START
;
;THE GAME STARTS HERE.
;--------------

G_INIT
	push iy
	di
	xor a				;Initialise our initial GMODE to Zero
	ld (GMODE),A
	ld (SPR_PBULLET+4),A
	ld (SPR_PBULLET+5),A		
					;set up interupts
	ld (sound_Flag),a
	ld (fleet_snd_reset),a
	ld HL,Interrupt
	LD IX,$FFF0
        LD (IX+04h),&C3            ; Z80 opcode for JP
        LD (IX+05h),L              ; Where to JP to (in HL)
        LD (IX+06h),H
        LD (IX+0Fh),&18            ; Z80 Opcode for JR
	ld a,$80
	ld i,a
	im 2
	ei
	CALL INIT			; Initialise all scores to zero 
	CALL PRINTS		; Set up the initial screen + colours
	call EDGES			; blacken the edges where the saucer 						; will appear
RESET	LD A,(GMODE)
	LD L,A
	LD H,0
	ADD HL,HL
	LD DE,JUMPTAB
	ADD HL,DE
	LD A,(HL)
	INC HL
	LD H,(HL)
	LD L,A
	OR H
	CALL NZ,GMODE_CALL
	LD HL,KEYFLAG
	BIT 7,(HL)
	JR Z,RESET
	pop iy

; Any interupts would need reset here
	di
	ld a,$3f
	ld i,a
	im 1
	ei
;kill the ay sound
	ld hl,kill_snd
	   ld b,8
	   call aysetreg
	RET

Interrupt:  DI                         ; Disable interrupts
            PUSH AF                    ; Preserve all registers
	    PUSH BC
            PUSH DE
            PUSH HL
            PUSH IX
            EXX
            EX AF,AF'
            PUSH AF
            PUSH BC
            PUSH DE
            PUSH HL
            PUSH IY
	    LD HL,(FRAMES)
	    INC HL
	    LD (FRAMES),HL
           CALL all_sounds
	            POP IY                     ; Restore all registers
	            POP HL
	            POP DE
	            POP BC
	            POP AF
	            EXX
	            EX AF,AF'
	            POP IX
	            POP HL
	            POP DE
	            POP BC
	            POP AF
	            EI                         ; Enable interrupts
	            RETI                       ; Return from interrupt

;--------------
; GAME MODES
; MODE 3 - Play Game
; Uses Subroutines
; ACTIVATEPLAY, THALT_PREP, CLR_FLSH, MOVER, FIRE_PREP
; BOMBS_DROP, SCANB, STATICS, SPR_DISP, THALT
;--------------

GMODE3  

	LD A,(PACTIVE)
	cp 0
	push af
	CALL Z,ACTIVATE_PLAY
	pop af
	call thalt_prep
	call clr_flsh
	CALL MOVER
	call nz,gmode3_upd ; a zero from mover doesnt want to update stuff.. to slow graphic gliches
	ex de,hl
	ld (flush_pointer),hl
	call spr_disp
	call thalt
	push ix
	ld a,(NUMALIENS)  
	ld ix,spr_saucer  ; Reorganise these sprites better to make this easier
	or (ix+0)
	LD IX,SPR_PLY_HIT
	or (ix+0)
	LD IX,SPR_SAUCER_SCORE
	or (ix+0)
	ld ix,BOMB_ARRAY
	ld b,num_bombs
	ld de,$0008
anybombs
	or (ix+4)
	or (ix+5)
	add ix,de
	djnz anybombs
	pop ix
	cp 0
	jp z,ALLDEAD
	ld a,(Gmode)
	cp 3
	ret nz
	LD HL,KEYFLAG
	BIT 7,(HL)
	JR Z,GMODE3
	RET
ALLDEAD 
	ld a,4
	ld (GMODE),a
	ret

gmode3_upd	

	call FIRE_PREP ; determines whats the lowest on each row
	call BOMBS_DROP ; DROPS THE ALIENS MISSINES
	CALL SCANB
	call statics
	ret
;--------------
; SCANB
; Part of GMODE 3 - Play Game
; Checks the key flag for the player 
; Uses Subroutines
; PL_SHOOT, PL_MLEFT, PL_MRIGHT
; saucer_init, saucer_move, ALIEN_FIRE_CONTROL. pl_fire
;--------------


SCANB	
	LD A,(KEYFLAG)
	PUSH AF
	BIT 2,A
	CALL NZ,PL_SHOOT ; PL_SHOOT
	POP AF
	PUSH AF
	BIT 1,A	
	CALL NZ,PL_MLEFT
	POP AF
	BIT 0,A
	CALL NZ,PL_MRIGHT
	call saucer_init
	call saucer_move
	CALL ALIEN_FIRE_CONTROL
	CALL pl_fire
	RET


;--------------
; PL_SHOOT
; Player shot routines
;--------------

PL_SHOOT  LD IX,SPR_PBULLET
	  LD A,(IX+4)
	  OR (IX+5)
	  JR NZ,NO_SHOOT ; ALREADY A SHOT SO SKIP

	  LD IX,SPR_PLY
	  LD B,(ix+4)
	  LD A,(IX+5)
	  SUB 5
	  LD C,A; GET THE coords OF PLAYER (X,Y-5)
	  LD IX,SPR_PBULLET
	  LD (IX+4),B
	  LD (IX+5),C  ; PUT INTO BULLET TO ACTIVATE IT
	PUSH IX
	LD A,$1F
	AND (IX+6)
	LD (IX+6),A
	POP HL
	INC HL
	INC HL
	LD BC,06
	LDIR

; initialise the Channel B missile sound for a new missile
	call missiles_snd

NO_SHOOT 
	  ret

;--------------
; PL_FIRE
; Fires player bullets up the screen
; uses routines KILLBULLET. Static_Used
;--------------

PL_FIRE  LD IX,SPR_PBULLET
	  LD A,(IX+4)
	  OR (IX+5)
	  RET Z ; no BULLET TO MOVE

	PUSH IX
	LD A,$1F
	AND (IX+6)
	LD (IX+6),A
	POP HL
	INC HL
	INC HL
	LD BC,06
	LDIR; remove EXISTING BULLET STARTED.
	DEC (IX+5)
	DEC (IX+5)
	DEC (IX+5)
	DEC (IX+5)
	LD A,(ix+5)
	CP 16
	JR C,KILLBULLET; ; REACHED TOP OF SCREEN
rcode	ld a,1
	rrca
	rrca
	rrca
	or (ix+6)
	ld (ix+6),a
	PUSH IX
	POP HL
	INC HL
	INC HL
	LD BC,06
	LDIR; redraw EXISTING BULLET

	; set a flag here saying a bullet is firing..
	ld a,(sound_flag)
	set 1,a ; BIT 1 is the missile hiss
	ld (sound_flag),a


	RET
KILLBULLET 

; reset a flag here saying the bullet isn't firing
	ld a,(sound_flag)
	res 1,a
	ld (sound_flag),a

	XOR A
	LD B,(IX+4)
	ld c,(ix+5)
	LD (IX+4),A
	LD (IX+5),A ; DELETE THE BULLET
	ld ix,SPR_BombTop
	ld a,(ix+0)
	cp 0
	call nz,Static_Used ; will delete any other static in use
			    ; and use this one instead.
	ld (ix+0),statics_duration
	ld (ix+4),b
	ld (ix+5),c
; set a flag here saying a static is used..

Static_Used push ix
	pop hl
        inc hl
	inc hl
	ld bc,6
	ldir
	RET

;--------------
; Player Movement Routines
; PL_MLEFT, PL_MRIGHT
; Moves player left and right to limits
;--------------


PL_MLEFT   LD A,(KEYFLAG)
	BIT 0,A
	RET NZ ; BOTH PRESSED
	LD IX,SPR_PLY
	LD A,(IX+4)
	DEC A
	CP limitl
	RET C ; OUT OF LIMITS
	PUSH IX
	POP HL
	INC HL
	INC HL
	LD BC,06
	PUSH HL
	PUSH BC
	LDIR
	LD (IX+4),A
	POP BC
	POP HL
	LDIR
	RET


PL_MRIGHT   LD A,(KEYFLAG)
	BIT 1,A
	RET NZ ; BOTH PRESSED
	LD IX,SPR_PLY
	LD A,(IX+4)
	INC A
	CP limitr
	RET NC ; OUT OF LIMITS
	PUSH IX
	POP HL
	INC HL
	INC HL
	LD BC,06
	PUSH HL
	PUSH BC
	LDIR
	LD (IX+4),A
	POP BC
	POP HL
	LDIR
	RET

;--------------
; ACTIVATE_PLAY
; 
; Sets up the player onto the play area after initialisation and death.
; Users routine SHOW_LIVESLEFT and Sprite control routines CLR_FLSH 
; and  SPR_DISP
;--------------

ACTIVATE_PLAY 
	ld a,(keyflag)
	and $0f
	ld (keyflag),a ; reenable the movement keys
	ld ix,firepattern ; reset the firepattern each time its called
	ld (ix+0),$0	  ; used to stop firing when player hit	
	ld (ix+1),$80
	LD A,1
	LD (PACTIVE),A
	call dp1lives
	LD IX,SPR_PLY
	LD HL,SHIP1
	LD DE,$A925
	LD BC,$0208
	LD (IX+2),L
	LD (IX+3),H
	LD (IX+4),E
	LD (IX+5),D
	LD (IX+6),C
	LD (IX+7),B
	INC IX
	INC IX	
	CALL CLR_fLSH ; SETS DE
	LD BC,06
	PUSH IX
	POP HL
	LDIR
	CALL SHOW_LIVESLEFT
	CALL SPR_DISP
	RET

SHOW_LIVESLEFT
	push de
	CALL SHIPNUM_CLR
	pop de
	LD A,(P1LIVES)
	dec a
	call nz,SHIPNUM_DISP
	RET

;--------------
; GAME MODES
; MODE 2- Prep for Player 1
; Uses Subroutines
; SHIPNUM_CLR, FILL_BOMBS, FLEET_INIT, FLEET_DISP
; SHIPNUM_DISP and sprite routines
;--------------

GMODE2  
	CALL SHIPNUM_CLR
	CALL DP1LIVES
	LD A,(P1LIVES)
	dec a
	CALL CLR_FLSH
	CALL SHIPNUM_DISP
	CALL SPR_DISP
	XOR A
	LD (NUMALIENS),A
	LD (CSECS),A
	LD (PACTIVE),A
	CALL FILL_BOMBS
	CALL FLEET_INIT
	CALL FLEET_DISP

	   ld hl,sound_flag
	   set 4,(hl) ; the first move sound!

	ld a,3
	ld (gmode),a
	RET

;--------------
; SHIPNUM_CLR
; Uses Subroutines
; PLOT
; clears the lower ship number icons
;--------------


SHIPNUM_CLR 	LD HL,$B91E
		LD B,6
SHIPNUM_LOOP	PUSH BC
		PUSH HL
		CALL PLOT
		PUSH DE
		POP HL
		INC DE
		LD BC,06
		XOR A
		LD (HL),A
		LDIR
		POP HL
		INC H
		POP BC
		DJNZ SHIPNUM_LOOP
		RET

;--------------
; SHIPNUM_DISP
; Fills buffer with ship icons for lives not in use icons.
; 
;--------------


SHIPNUM_DISP	
	PUSH DE	
	POP IX
	LD HL,$B91E
	LD B,A
SHIPS_LOOP PUSH BC
	LD DE,$000E
	ADD HL,DE
	LD DE,SHIP1
	LD BC,$0206
	LD (IX+0),E
	LD (IX+1),D
	LD (IX+2),L
	LD (IX+3),H
	LD (IX+4),C
	LD (IX+5),B
	LD DE,06
	ADD IX,DE
	POP BC
	DJNZ SHIPS_LOOP
	RET

;--------------
; GMODE1
; Player 1 starts game clears screen and flashes scores prep
;--------------

GMODE1	CALL CLG
	ld a,$05
	ld (show_flag),a
	XOR A
	LD (LSECS),A
	LD (CSECS),A
	LD (BCDS1),A
	LD (BCDS1+1),A
	LD (BCDS1+2),A
	LD A,(CREDITS)
	DEC A
	DAA
	LD (CREDITS),A
	CALL DCREDIT
	LD A,3
	LD (P1LIVES),A
GM1L	
	CALL TIMER
	CALL DS3
	CALL DEMOKB
	LD A,(GMODE)
	CP 1
	RET NZ
	BIT 7,(HL)
	JR Z,GM1L
	RET

DS3	LD A,(CSECS)
	CP 19
	JR NC,DS3_2
	LD HL,0608H
	LD BC,0600H
	LD DE,PLAYER1S1
	CALL STRINGZ
	LD A,(CSECS)
	SRL A
	CALL C,BLANK_SCORE1
	CALL NC,UPDATES
	CALL BLANK_SCORE2
	LD A,(CSECS)
	CP 18
	CALL Z,CLG
	RET
DS3_2	
; used to reset after a full fleet clear.
	CALL BOTL
	call SHOWBASE
	LD A,2
	LD (GMODE),A
	RET

BLANK_SCORE1
	PUSH AF
	LD HL,0104H
	LD DE,BSCORES
	CALL STRINGZ
	POP AF
	RET

BLANK_SCORE2
	PUSH AF
	LD HL,0116H
	LD DE,BSCORES
	CALL STRINGZ
	POP AF
	RET

;--------------
; GMODE5
; Reset the game after a full alien clear
; jump back to GMODE3
;--------------

GMODE5	CALL CLG
	XOR A
	LD (LSECS),A
	LD (CSECS),A
	ld IX,SPR_PBULLET
	ld (ix+4),a
	ld (ix+5),a
	CALL FILL_BOMBS  ; clears all alient bombs
	CALL RESET_STATICS ; clears all static sprites

	JP DS3_2

;--------------
; GMODE4
; Player has cleared the fleet - win announcement
; jumps to reinforcements prep
;--------------

GMODE4  XOR A
	LD (LSECS),A
	LD (CSECS),A
	LD (SPR_SAUCER),A	
gm4LOOP	;HALT ; wait for the screen thing
	CALL TIMER
	CALL ALIENSGONE
	CALL DEMOKB
	LD A,(gmode)
	cp 5
	ret z
	LD HL,KEYFLAG
	BIT 7,(HL)
	JR Z,gm4LOOP
	RET

ALIENSGONE	LD A, (CSECS)
		cp $50
		jr z,alienzreset
		CP 01H
	CALL Z,CLG
	LD HL,0F04H
	LD BC,1000H
	LD DE,YOUWIN
	CALL STRINGT
	RET

alienzreset	ld a,5
	ld (gmode),a
	ld a,(numfleet)
	inc a
	cp 9
	jr c,newdownok
	xor a
newdownok:
	ld (numfleet),a
	ret
	
YOUWIN DB "YOU WIN ...... THIS TIME.",0

;--------------
; GMODE6
; Game over alien invasion returns to GMODE0
;--------------

GMODE6  CALL UPD_HISCORE
	XOR A
	LD (LSECS),A
	LD (CSECS),A
	LD (SPR_SAUCER),A	
gm6LOOP	
	CALL TIMER
	CALL INVADED
	CALL DEMOKB
	LD A,(gmode)
	cp 0
	ret z
	LD HL,KEYFLAG
	BIT 7,(HL)
	JR Z,gm6LOOP
	RET

INVADED	LD A, (CSECS)
		cp $80
		jr z,select_gmode0
		CP 01H
	CALL Z,CLG
	LD HL,0F04H
	LD BC,1000H
	LD DE,BEENINVADED
	CALL STRINGT
	RET

select_gmode0 xor a
	ld (gmode),a
	call clg
	ret
	
BEENINVADED DB "GAME OVER. ALIENS INVADED.",0


;--------------
; GMODE7
; Game over out of lives
;--------------

GMODE7  call dp1lives
	CALL UPD_HISCORE
	XOR A
	LD (LSECS),A
	LD (CSECS),A
	LD (SPR_SAUCER),A	
gm7LOOP	
	CALL TIMER
	CALL NOLIVES
	CALL DEMOKB
	LD A,(gmode)
	cp 0
	ret z
	LD HL,KEYFLAG
	BIT 7,(HL)
	JR Z,gm7LOOP
	RET

NOLIVES	LD A, (CSECS)
	cp $80
	jp z,select_gmode0
	CP 01H
	CALL Z,CLG
	LD HL,0F04H
	LD BC,1000H
	LD DE,OUTOFLIVES
	CALL STRINGT
	RET

	
OUTOFLIVES DB "NO LIVES LEFT. GAME OVER. ",0

;--------------
; GMODE0
; Initial information screen awaiting input of credits
; and selection of start  (only 1 player works )
;--------------


GMODE0  XOR A
	LD (FRAMES),A
	LD (LSECS),A
	LD (CSECS),A
	ld a,$07
	ld (show_flag),a

DLOOP	
	CALL TIMER
	CALL DS1
	CALL DEMOKB
	LD A,(LSECS)
	LD H,A
	LD A,(CSECS)
	CP H
	JR Z,NOTTIME
	LD A,(CSECS)
	LD (LSECS),A
NOTTIME	LD HL,KEYFLAG
	BIT 0,(HL)
	JP Z,NOPLAY
	LD A,(CREDITS)
	AND A
	JR Z,NOPLAY
	LD A,1
	LD (GMODE),A
	RET
NOPLAY	BIT 1,(HL)
	CALL NZ,UPCREDIT
	BIT 7,(HL)
	JR Z,DLOOP
	RET

REST1	; OKAY 1 PRESSED IS THERE ANY CREDITS?
	XOR A
	LD (CSECS),A
	LD (LSECS),A
	CALL CLG
	RET

;--------------
; Timing routines
;
;--------------

FRAMES EQU 5C78H

OLDFRAMES DB 0
LSECS DB 0
	nop
CSECS DB 0

;--------------
; thalt_prep
; stores the current fram in a variable
; for screen FPS
;--------------


thalt_prep push af
	push hl
	ld a,(FRAMES)
	ld hl,oldframes
	ld (hl),a 
	pop hl
	pop af
	ret

;--------------
; thalt
; for screen FPS
;--------------


thalt	push af
	push hl
tdelay	
	ld hl,oldframes
	ld a,(FRAMES)
	sub (hl)
	cp 2  ; 1 is insanely fast...
	jp c,tdelay
	CALL gameKB
	LD a,(CSECS)  ; for animations
	inc a
	ld (CSECS),a
	pop hl
	pop af
	ret


;--------------
; Timed and timed string routines
; TIMER
; checks frames for time delay of 8 20ms periods
; used for updating text on information screens
;--------------


TIMER   LD HL,OLDFRAMES
	LD A,(FRAMES)
	SUB (HL)
	cp 8
	jp nc,textdelay
	ret
textdelay	
	ld hl,OLDFRAMES
	ld a,(FRAMES)
	ld (hl),a
	LD A,(CSECS)
	INC A
	LD (CSECS),A
	RET

;--------------
;STRINGT
; POKE NULL TERMINATED STRING ONTO SCREEN AT LOCATION
; ENTER HL COLUMN ROW ; DE STRING LOCATION
; TIMER FUNCTION USED B IS NUMBER OF CHARACTERS - 
; IF  B IS EXCEEDED ZERO STRING USED
;--------------

STRINGT	LD A,(CSECS)
	CP B
	RET C
	LD A,C
	AND A
	JR NZ,NOVIDI
	LD A,(CSECS)	
	SUB B
	LD C,A
NOVIDI	PUSH BC
	PUSH DE
	CALL SADDR
	POP DE
	EX DE,HL
	POP BC
StLOOP	LD A,C
	AND A
	RET Z
	LD A, (HL)
	CP 0
	RET Z
	PUSH HL
	PUSH BC
	CALL CHRPOKE
	POP BC
	POP HL
	INC HL
	INC DE
	DEC C
	JR StLOOP

;--------------
; gPOKE
; --------------
gPOKE	LD B,8
gLOOP	push de
	LD A,(HL)
	LD (DE),A	
	INC HL
	INC DE
	ld a,(hl)
	ld (de),a
	inc hl
	pop de
	inc d
	DJNZ gLOOP
	RET

;--------------
; Display Screen Messages
;--------------

INTRO1	DB "THE SPACE INVADERS",0
INTRO2	DB "PRESENTS",0
INTRO3  DB "................",0
INTRO4  DB "- ZX  INVADERS -",0
INTRO5 DB "*SCORE ADVANCE TABLE*",0
INTRO6 DB "= ? MYSTERY",0
INTRO7 DB "= 30 POINTS",0
INTRO8 DB "= 20 POINTS",0
INTRO9 DB "= 10 POINTS",0
INTROA DB "A TRIBUTE TO A CLASSIC",0
INTROB DB "CODED BY THE GRID",0
INTROC DB "PRESS 5 TO INSERT CREDITS",0
INTROD DB "PRESS 1 OR 2 PLAYER TO PLAY",0
INTROE DB "5=LEFT, 8-RIGHT, 0=FIRE",0
;--------------
; DS1 - Display Screens
; used by GMODE0
; uses non rom printing routines, timed printing routines
; and ALIENS to display graphics on page 1
;--------------

DS1	LD A,(CSECS)
	AND A
	push af
	CALL Z,CLG
	pop af
	CP 60H
	JP NC, PAGE2
	LD HL,0207H
	CALL SADDR
	INC H
	INC H
	INC H
	INC H
	LD DE,INTRO1
	CALL STRINGZ2
	LD HL,040CH
	LD DE,INTRO2
	CALL STRINGZ
	LD HL,0608H
	LD BC,0600H
	LD DE,INTRO4
	CALL STRINGT
	LD HL,0906H
	LD BC,18FFH
	LD DE,INTRO5
	CALL STRINGT
	LD A,(CSECS)
	CP 25
	CALL NC,ALIENS
	LD HL,0B0DH
	LD BC,1A00H
	LD DE,INTRO6
	CALL STRINGT

	LD HL,0D0DH
	LD BC,2500H
	LD DE,INTRO7
	CALL STRINGT

	LD HL,0F0DH
	LD BC,3200H
	LD DE,INTRO8
	CALL STRINGT

	LD HL,110DH
	LD BC,4000H
	LD DE,INTRO9
	CALL STRINGT
	RET

PAGE2	LD A, (CSECS)
	CP 60H
	CALL Z,CLG

	LD HL,0406H
	LD DE,INTROE
	CALL STRINGZ

	LD HL,0F04H
	LD BC,61FFH
	LD DE,INTROC
	CALL STRINGT
	LD HL,1103H
	LD BC,6200H
	LD DE,INTROD
	CALL STRINGT
	LD HL,0806H
	LD BC,70FFH
	LD DE,INTROA
	CALL STRINGT
	LD HL,0A09H
	LD BC,7500H
	LD DE,INTROB
	CALL STRINGT
	LD A,(CSECS)
	CP 0FFH
	CALL Z, CLG
	RET

;--------------
; ALIENS
; displates ALIENS graphics on page 1 GMODE0
;--------------

ALIENS  LD HL,0B08H
	CALL SADDR
	LD DE,SAUCER
	EX DE,HL
	CALL gPOKE
	LD HL,0D08H
	CALL SADDR
	LD DE,tALIEN0
	EX DE,HL
	CALL gPOKE
	LD HL,0F08H
	CALL SADDR
	LD DE,mALIEN0
	EX DE,HL
	CALL gPOKE
	LD HL,1108H
	CALL SADDR
	LD DE,bALIEN0
	EX DE,HL
	CALL gPOKE
	RET


;--------------
; CLG
; clears the play area display
;--------------

CLG	PUSH BC
	PUSH DE
	PUSH HL
	LD B,0A7H
	LD HL,0200H
	CALL SADDR
clGLOOP	PUSH BC
	PUSH HL
	PUSH HL
	POP DE
	INC DE
	LD BC,001FH
	LD (HL),00H
	LDIR
	POP HL
	INC H
	LD A,H
	AND 07
	JR NZ,GEND
	LD A,L
	ADD A,20H
	LD L,A
	CCF
	SBC A,A
	AND 0F8H
	ADD A,H
	LD H,A
GEND	POP BC
	DJNZ clGLOOP
	LD A,(CSECS)
	INC A
	LD (CSECS),A
	POP HL
	POP DE
	POP BC
	RET

;--------------
; Graphic Buffered Routines
; gBUFF
;GRAPHICS BUFFER
;24 bytes to store all graphics in non rotated state
;--------------
gBUFF 
	ds 24
	
;ROTATE GRAPHICS FROM STORE MEMORY TO CORRECT BIT MAP LOCATION FOR DISPLAY

ROTATE3  
	PUSH BC ; SAVE THE C AND THE B
ROTATER PUSH HL
	RR (HL)
	INC HL
	RR (HL)
	INC HL
	RR (HL)
	INC HL
	POP HL
	DJNZ ROTATER
	INC HL
	INC HL
	INC HL
	POP BC
	DEC C
	JR NZ,ROTATE3
	RET

;fBUFF
; fill the GBUFF are with the DEFAULT SPRITE TO ALTER SCREEN
; 
; always 2 chars width c = chars pixel lines down
; de - sprite data

fBUFF	push bc
	push de
	push hl
	ld b,c
	ld hl,gBUFF
	ex de,hl
	ld c,0ffh
gzloop	ldi
	ldi
	xor a
	ld (de),a
	inc de
	djnz gzloop
	pop hl
	pop de
	pop bc
	ret

;fixbuff moves fBUFF STORE graphic to correct pixel locations
; pixel location is based on the L ( x coord )
fixBUFF push bc
	  push de
	  push hl
	LD A,L
	  AND 7
	 JR Z, UNMOVED
;	 LD C,8
	 LD B,A
	 LD HL,gBUFF
	 CALL ROTATE3
unmoved	pop hl
	pop de
	pop bc
	ret
	
;sprPOKE
; POKE A SPRITE  DIRECTLY ON THE SCREEN
; HL - SCREEN COORDS
; DE - BITMAP
; B special counter or something
; c - PIXELS down
; ADJUSTED TO ALLOW FOR CROSS CELL PRINTING.

sprXOR	
	; ld bc,0208h ( trying different height sprites )
	push bc
	push bc
	call fbuff
	call fixbuff	
	CALL PLOT ; DE NOW SCREEN ADDRESS
	DEC DE
	pop bc
	ld b,3
	ld hl,gBUFF
	call g_XOR
	pop bc
	RET

sprNAND	
	; ld bc,0208h ( trying different height sprites )
	push bc
	push bc
	call fbuff
	call fixbuff	
	CALL PLOT ; DE NOW SCREEN ADDRESS
	DEC DE
	pop bc
	ld b,3
	ld hl,gBUFF
	call g_NAND
	pop bc
	RET


sprXOR_cd	

	push bc
	push bc
	call fbuff
	call fixbuff	
	CALL PLOT ; DE NOW SCREEN ADDRESS
	DEC DE
	pop bc
	ld b,3
	ld hl,gBUFF
	call g_XOR_cd
	ld a,c
	and $FF
	call nz,FLEET_COLLISION
	pop bc
	RET





; CODE FOR DISPLAYING THE BUILDINGS

SHOWBASE

	ld b,4
	LD HL,$9020

sb_loop push bc
	push hl
	CALL PLOT
	LD HL,BBASET
	ld bc,$0310
	call g_XOR
	pop hl
	ld bc,$0038
	add hl,bc
	pop bc
	djnz sb_loop
	RET

g_XOR_cd	ld a,c
        ld b,a
	ld c,0
XORCD_LOOP	
	PUSH DE	
	LD A,(DE)
 	and (hl)
	or c
	ld c,a
	ld a,(de)
	xor (hl)
	LD (DE),A
	INC HL	
	INC DE

	LD A,(DE)
 	and (hl)
	or c
	ld c,a
	ld a,(de)
	xor (hl)
	LD (DE),A
	INC HL	
	INC DE

	LD A,(DE)
 	and (hl)
	or c
	ld c,a
 	ld a,(de)
	xor (hl)
	LD (DE),A
	INC HL	
	INC DE
	pop de
	INC D
	LD A,D
	AND 07
	JR NZ,XORCD_OK
	LD A,E
	ADD A,20H
	LD E,A
	CCF
	SBC A,A
	AND 0F8H
	ADD A,D
	LD D,A
XORCD_OK	
	djNZ XORCD_LOOP
	ret


g_XOR	ld a,c
        ld b,a
	ld c,0
XOR_LOOP	
	PUSH DE	
	ld a,(de)
	xor (hl)
	LD (DE),A
	INC HL	
	INC DE
	ld a,(de)
	xor (hl)
	LD (DE),A
	INC HL	
	INC DE
 	ld a,(de)
	xor (hl)
	LD (DE),A
	INC HL	
	INC DE
	pop de
	INC D
	LD A,D
	AND 07
	JR NZ,XOR_OK
	LD A,E
	ADD A,20H
	LD E,A
	CCF
	SBC A,A
	AND 0F8H
	ADD A,D
	LD D,A
XOR_OK	
	djNZ XOR_LOOP
	ret
;----

g_NAND	ld a,c
        ld b,a
	ld c,0
NAND_LOOP	
	PUSH DE	

	ld a,(de)
	ld c,a
	ld a,(hl)
	cpl
	and c
	LD (DE),A
	INC HL	
	INC DE

	ld a,(de)
	ld c,a
	ld a,(hl)
	cpl
	and c
	LD (DE),A

	INC HL	
	INC DE

	ld a,(de)
	ld c,a
	ld a,(hl)
	cpl
	and c
	LD (DE),A

	INC HL	
	INC DE
	pop de
	INC D
	LD A,D
	AND 07
	JR NZ,NAND_OK
	LD A,E
	ADD A,20H
	LD E,A
	CCF
	SBC A,A
	AND 0F8H
	ADD A,D
	LD D,A
NAND_OK	
	djNZ NAND_LOOP
	ret




;-----
;SPR_DEF Sprite Definitions
SPR_PLY	DW 0; STATUS BYTES
	dw SHIP1
	DW $A825
	DW $0208

SPR_PLY_HIT	DW 0; STATUS BYTES
	dw SHIP1_HIT
	DW $A825
	DW $0208

;SPR_DEF Sprite Definitions

;SPR_DEF Sprite Definitions
SPR_BDAMAGE	DW 0; STATUS BYTES
		dw gBLAST
		DW $0000
		DW $0248 ; $4 = drawmode NAND

SPR_BombTop	DW 0; STATUS BYTES
		dw gBlast
		DW $0000
		DW $0206

SPR_Explode	DW 0; STATUS BYTES
		dw gaBoom
		DW $0000
		DW $0208

SPR_BOMB dw 0
	 dw gaSHOT1
	 dw $0000 ; xcoord
	 dw $0204 ; width, height + special bits in width

SPR_SAUCER_DESTROY Dw $0000
	   DW SAUCER_DESTROY
           DW $0810
	   DW $0208

SPR_SAUCER_SCORE Dw $0000
	   DW SAUCER_300
           DW $0810
	   DW $0208


SPR_PBULLET	DW 0; STATUS BYTES
		dw gpBULLET
		DW $0000
		DW $0204


statics_duration equ 5

; statics are sprites that appear for short durations
; ie exploding bombs and missiles and killed aliens.
STATICS         LD IX,SPR_BombTop
		CALL Check_Statics
		LD IX,SPR_Explode
		CALL Check_Statics
		LD IX,SPR_SAUCER_DESTROY
		CALL Check_Statics_Saucer
		LD IX,SPR_SAUCER_SCORE
		CALL Check_Statics
		call STATIC_PL_HIT
		RET

RESET_STATICS	XOR A
		LD IX,SPR_BombTop
		LD (IX+0),a
		LD IX,SPR_Explode
		LD (IX+0),a
		LD IX,SPR_SAUCER_DESTROY
		LD (IX+0),a
		LD IX,SPR_SAUCER_SCORE
		LD (IX+0),a
		LD IX,SPR_Explode
		LD (IX+0),a
		LD IX,SPR_PLY_HIT
		LD (IX+0),a

		RET

; this routing will overright the static sprite on value 1, 
; ignore it at zero 
Check_Statics	
		ld a,(ix+0)
		cp 0
		ret z
		cp 1
		jr nz,SKIP
		push ix
		pop hl
		inc hl
		inc hl
		ld bc,6
		ldir
SKIP		dec (ix+0)		
		ret

;this routine will check a static with animation
; player destroyed ...

Check_Statics_animate_pl_destry
		ld a,(ix+0)
		cp 0
		ret z
		cp 1
		jr z,last_one
		push ix
		pop hl
		inc hl
		inc hl
		ld bc,6
		ldir
		ld a,(csecs)
		and 1
		ld h,0
		ld l,a
		add hl,hl
		add hl,hl
		add hl,hl
		add hl,hl
	        ld bc,SHIP1_HIT
	        add hl,bc
	        ld (ix+2),l
	        ld (ix+3),h
last_one	push ix
		pop hl
		inc hl
		inc hl
		ld bc,6
		ldir
		dec (ix+0)		
		ret

;this routine will check if the saucer static 
; is active and move it across the screen

Check_Statics_Saucer ; scoring for saucer here as well
		ld a,(ix+0)
		cp 0
		ret z
		CP 5
		CALL Z,saucer_random_score
		cp 1
		jr nz,SKIP_sauc
		push ix
		pop hl
		inc hl
		inc hl
		ld bc,6
		ldir
SKIP_Sauc	dec (ix+0)

		ret

PLAYER_HIT	db 0

STATIC_PL_HIT   ld hl,keyflag
		bit 6,(hl)
		ret z
		ld a,(PLAYER_HIT)
		and a
		call z,STATIC_PL_HIT_INIT ; hit recognised for the first time
		
		LD IX,SPR_PLY_HIT
		ld a,(ix+0)
	        cp 1
		jr nz,PL_HIT_KEEP_ENABLED
	        xor a
		ld (player_hit),a
		ld (keyflag),a
		ld (pactive),a
		ld a,(p1lives)
		dec a
		ld (p1lives),a
		cp 0
		jr nz,PL_HIT_KEEP_ENABLED
		ld a,7
		ld (gmode),a
		ret
PL_HIT_KEEP_ENABLED 

		call Check_Statics_animate_pl_destry
		ret

STATIC_PL_HIT_INIT
	     ld a,1
	     ld (PLAYER_HIT),a

; pick up player coords
	     LD IX,SPR_PLY
	     ld h,(ix+5)
	     ld l,(ix+4)

;store in hit graphic
	     LD IX,SPR_PLY_HIT
	     ld (ix+5),h
	     ld (ix+4),l

;display hit graphic
	     PUSH IX
	     POP HL
	     INC HL
	     INC HL
	     LD BC,6
	     LDIR

;set the static delay	
	     ld (ix+0),player_death ; not sure how long this to be....
;erase player graphic	     
	     LD IX,SPR_PLY
	     PUSH IX
	     POP HL
	     INC HL
	     INC HL
	     LD BC,6
	     LDIR
	     ret


saucer_random_score  

		push ix
		push de
		xor a
		ld ix,SPR_SAUCER_DESTROY
		ld a,(ix+4) ; using destroy point as a random element
		and 3
		cp 1 
		jr z,sel_150
		cp 2
		jr z,sel_200
sel_300		ld hl,SAUCER_300
		ld (SPR_SAUCER_SCORE+2),hl
		ld h,3
		ld l,0
		jr do_saucer_score

sel_150		ld hl,SAUCER_150
		ld (SPR_SAUCER_SCORE+2),hl
		ld h,1
		ld l,$50
		jr do_saucer_score

sel_200		ld hl,SAUCER_200
		ld (SPR_SAUCER_SCORE+2),hl
		ld h,2
		ld l,0

do_saucer_Score
		xor a
		ld (credit+1),a
		ld a,l
		ld (digit+1),a
		ld a,h
		ld (hundred+1),a
		ld hl,BCDS1+2
		call bcdadd
		pop de
		ld ix,SPR_SAUCER_DESTROY
		ld h,(ix+4)
		ld l,(ix+5)
		ld ix,SPR_SAUCER_SCORE
		ld (ix+5),l
		ld (ix+4),h
		ld (ix+0),20
		push ix
		pop hl
		inc hl
		inc hl
		ld bc,6
		ldir
		pop ix
		ret

		
SPR_SAUCER Dw $0000
	   DW SAUCER
           DW $0810
	   DW $0208

SPR_ALIEN DW 0;STATUS BYTES
	  dw bALIEN0
	  dw $2020
	  dw $0208

;fleet data.  11 rows by 5 columns x 8 data fields
; data structre is from above
; dw - status byte and animation counter
; dw - sprite pointer
; dw - coordinates on screen
; dw - initial width and height ( note widths is coded to 
; always be 2 bytes wide so this is used as a special code in the
; graphics buffer.

	 
ALIEN_FLEET 
		DS 441 ; 440 bytes for fleet + 1 space bit.

;SPR_DISP- display sprite data to screen.
SPR_DISP 	 ; this is a blunt instrument - remove it and it plays faster.
	ld ix,GFLUSH
	ld e,(ix+0)
	ld d,(ix+1)
	ld a,d
	or e
	jp nz,updater
	ret
updater  
	ld l,(ix+2)
	ld h,(ix+3)
	ld c,(ix+4)
	ld b,(ix+5)
	ld a,c
	and $e0
	rlca
	rlca
	rlca
	ld b,a
	ld a,c
	and $1f
	ld c,a
	ld a,b
	push af
	cp 0
	call z, sprXOR ; just put it on the screen
	pop af
	push af
	cp 1
	call z, sprXOR_CD ; player shot - put on screen and check for collision
	pop af
	push af
	cp 2
	call z, sprNAND ; NAND a sprite onto screen used for player shot destroying buildings
	pop af
	push af
	cp 3
	call z, sprXOR_CD_BOMBS ; provision for alien bombs put on screen and check for collision 
	pop af
	ld bc,06
	add ix,bc
	ld e,(ix+0)
	ld d,(ix+1)
	ld a,d
	or e
	jp nz,updater
	ret

; CLEAR THE GRAPHICS BUFFER
CLR_FLSH push bc
	LD HL,GFLUSH
	ld (flush_pointer),hl
         LD de,gflush+1
	 LD BC,$00FF
	ld (HL),0
	 LDIR
	pop bc
	LD DE,GFLUSH
	 RET

flush_pointer dw 0
fleet_move db 5

move_cycle db 0 ; current move 
chg_dor	  db 0  ; - edge hit change dor next time all moved

moverfix  dec a
	  ret
mover   
	ld a,(numaliens)
	cp 0
	jr z,moverfix
	ld ix,ALIEN_FLEET
	ld bc,$0b05
moverl	ld a,(ix+0)
	cp 0
	jr z,donem ; its dead ignore it
	ld a,(move_cycle)
	cp (ix+1)
	jr nz,donem
	ld a,(fleet_move)
	cp (ix+0)
	jp z, outandmover
donem	ld de,8
	add ix,de
	djnz moverl
	ld b,11
	dec c
	jr nz,moverl
; if it reaches here they have all moved right and a limit needs to be checked for down next...

resetm

	ld a,(sound_flag)
	set 4,a
	ld (sound_flag),a

	ld a,(move_cycle)
	inc a
	ld (move_cycle),a
	ld a, (fleet_move)
	cp 6
	jr z,force_dor
	cp 8
	jr z,force_dor
	ld a,(chg_dor)
	and a
	jp z,no_dor
force_dor ld a, (fleet_move)
	inc a
	cp 9
	jr nz ,nofix
	ld a,5
nofix   ld (fleet_move),a
	call adjust_fleet
no_dor	

	ret

adjust_fleet 	
; a is the new fleet mode
	push ix
	push hl
	ld h,a
	ld a,(numaliens)
	cp 1
	jr z,special_adjust
	ld ix,ALIEN_FLEET
	ld bc,$0b05
	ld de,8
adjl	ld a,(ix+0)
	cp 0
	jr z,donowt
	ld (ix+0),h
donowt	add ix,de
	djnz adjl
	ld b,11
	dec c
	jr nz,adjl

	xor a
	ld (chg_dor),a

	pop hl
	pop ix
	ret

special_adjust 

	xor a
	ld (chg_dor),a

		ld a,h
	       pop hl
	       pop ix
	       ld (ix+0),a
		ret


; out and move is called if a move ship is found
;C is the row it is on
outandmover push ix
           pop de  
	   ld a,c
	   ld hl,gflush
	   ex de,hl
	   inc hl	
	   inc hl
	   push hl
	   push af
	   ld bc,06
	   ldir ; move the copy to erase current graphic
	   inc (ix+1) ; Say that its moved
           ld a,(fleet_move)
	   cp 5
	   jr nz, rightno
	   inc (ix+4) ; move it one to the right
	   inc (ix+4) ; move it one to the right
rightno    cp 6
	   jr nz,down1no
	   inc (ix+5) ; move it down 
	   inc (ix+5) ; move it down 
down1no    cp 7
	   jr nz,leftno
	   dec (ix+4)
	   dec (ix+4) ; move it one to the left	
leftno	   cp 8
	   jr nz,down2no
	   inc (ix+5) ; move it down 
	   inc (ix+5) ; move it down 

down2no

;graphics selection
; a contains c
	pop af
	   and a
  	   cp 1
	   jp nz,skip1
	   ld hl,tALIEN0
	   jr skipout
skip1	   cp 4
	  jr nc, skip2	
         ld hl,mALIEN0
	   jr skipout
skip2      ld hl,bALIEN0
skipout    ld a,(ix+1) ; using movecycle as graphic select
	   and 01
	   jr z, graphicok
	   ld bc,16
           add hl,bc
graphicok   ld (ix+2),l
	    ld (ix+3),h
	ld a,(ix+4)
        cp limitr
	jr c,norlim
	ld a,1
	ld (chg_dor),a
norlim  cp limitl
	jr nc,nollim
	ld a,1
	ld (chg_dor),a
nollim	   ld a,(ix+5)
	   cp limitd ; compare with the lower limit
	   jr c,nodlim
	ld a,6
	ld (gmode),a ; GMODE 6 invaded
nodlim
	   pop hl
	   ld bc,6
	   ldir ; put in the new graphics

	ld a,(numaliens)
	cp 1
	ret nz ; return if more than one alien about
	push af
; if there is only one we should check limits right away.
	call resetm
	pop af
	cp 0
	ret nz ; and this is the fix so we get our nz exit

; this was the original fix when the animations stopped with one guy left.
	ld hl,csecs
	dec (hl)
;	call spr_disp
	ret	
	



;FLEET_DISP Show the fleet
; requires a check for alive if we want to do 1 and 2 player modes.
FLEET_DISP
	ld b,55
iniyl   push bc
	call clr_flsh
	ld a,55
	sub b
	ld l,a
	ld h,0
	LD DE,GFLUSH
	LD BC,ALIEN_FLEET
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,bc
	ld a,(hl)
	and a
	jr z,ignore_dead
	INC HL
	inc HL
	ld BC,06
	LDIR
ignore_dead
	XOR A
	EX DE,HL
	LD (HL),A
	INC HL
	LD (HL),A	
	INC HL
	EX DE,HL
	CALL SPR_DISP
	pop bc
	djnz iniyl
	RET

;NUmber of alien fleets there has been. ( max 7 - resets at 8)
NUMFLEET DB 0
;ALIENT FLEET INITIALISATION.
FLEET_INIT   	xor a
	ld (move_cycle),a 
	ld a,5
	ld (fleet_move),a
	push ix
	ld ix, SPR_ALIEN
	ld a,$20
	ld (ix+4),a
	ld (ix+5),a
	ld a,(numfleet)
	add a,a ;x2
	add a,a ;x4
	add a,(ix+5)
	ld (ix+5),a
	pop ix
	LD DE,ALIEN_FLEET
	ld bc,$0b05 ; b -11, c=5
fleetloop
	push bc
	LD HL,SPR_ALIEN
	ld BC,08
	LDIR
	pop bc ; get bc back
	push bc
	push de
	ex de,hl
	ld de,08
	sbc hl,de ; start of block
	push hl
	pop ix ; ix start of block

	ld a,c
	cp 1
	jr nz, not_top
top	ld hl,tALIEN0
        jp gselected
not_top cp 4
	jp nc, bot_row
mid_rom ld hl,mALIEN0
	jp gselected
bot_row ld hl,bALIEN0
gselected ld (ix+2),l
	ld (ix+3),h

; fixes the start coordinates of all the aliens
	ld a,11
	sub b
	add a,a ;x2
	ld d,a; store x4
	add a,a  ;x4
	add a,a ;x8
	add a,a; x16
	add a,d ;x18
	ld e,a
	ld a,(ix+4)
	add a,e
	ld (ix+4),a
	ld a,c
	add a,a ;x2
	add a,a; x4
	ld d,a ; store x4
	add a,a ;x8
	add a,d ; x12
	ld e,a
	ld a,(ix+5)
	add a,e
	ld (ix+5),a
	ld (ix+0),5 ; set up for initial move
	ld (ix+1),0
	pop de
	pop bc
	djnz fleetloop
	ld b,11
	dec c
	jr nz, fleetloop
	LD A,55
	ld (numaliens),a
	RET


;FLEET_COLLISION Checks the fleet with a collision for the players bullet

FLEET_COLLISION
	push ix
	ld ix,SPR_PBULLET   ; ix+4  player x , ix+5 player y
	ld b,55
coll_l   ld a,55
	sub b
	ld l,a
	ld h,0
	LD DE,ALIEN_FLEET
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,DE ; hl points to the fleet entry hl+4 = fleet x , hl+5 = fleet y
	ld a,(hl)
	and a
	jr z,ship_isdead
	ld de,4
	add hl,de
	ld a,(hl) ; x already middle of sprite
	ld d,a
	inc hl
	ld a,(hl) ; y is 4 below mid ( screen invetered)
	add a,4
	ld e, a	  ;	(d,e) now fleets (x,y) to check
	ld a,(ix+4) ; bullet already mid position
	ld h,a
	ld a,(ix+5) ; bullet 2 below mid
	add a,2
	ld l,a ; now the bullets (x,y) to check
	CALL COL8x8 ; returns carry if any overlap
	JP C,KILL_FLEET_SHIP
ship_isdead	
	djnz coll_l

;if we get here(h,l) still refreshed
	ld ix,SPR_PBULLET   ; ix+4  player x , ix+5 player y
	ld a,(ix+4) ; bullet already mid position
	ld h,a
	ld a,(ix+5) ; bullet 2 below mid
	ld l,a ; now the bullets (x,y) to check

build_ck	ld d,44
	ld e,152; (d,e) hold mid of 1st building

	call ALLBUILDING
	jp c,destroy_build
	ld d,100
	call ALLBUILDING
	jp c,destroy_build

	ld d,156
	call ALLBUILDING
	jp c,destroy_build

	ld d,212
	call ALLBUILDING
	jp c,destroy_build

;ok here we refresh for saucer coll det

	ld ix,SPR_SAUCER
	xor a
	cp (ix+0)
	jr z,end_fleet_coll   ; ix+4  player x , ix+5 player y

	ld a,(ix+4) ; bullet already mid position
	ld d,a
	ld a,(ix+5) ; bullet 2 below mid
	add a,2
	ld e,a ; now the bullets (x,y) to check

	ld ix,SPR_PBULLET   ; ix+4  player x , ix+5 player y
	ld a,(ix+4) ; bullet already mid position
	ld h,a
	ld a,(ix+5) ; bullet 2 below mid
	add a,2
	ld l,a ; now the bullets (x,y) to check

	call SAUCER_COL_DET
	jp c,destroy_saucer

end_fleet_coll
	pop ix
	RET

destroy_what dw 0

destroy_saucer
	ld hl,(flush_pointer)
	ex de,hl
	ld ix,SPR_PBULLET
	push ix
	pop hl
	inc hl
	inc hl
	ld bc,6
	ldir
; kills the bullet 
; reset a flag here saying the bullet isn't firing
	ld a,(sound_flag)
	res 1,a
	ld (sound_flag),a
	xor a
	ld (ix+4),a
	ld (ix+5),a
	
	ld ix,SPR_SAUCER
	ld h,(ix+4)
	ld l,(ix+5)
	ld ix,SPR_SAUCER_DESTROY
	ld (ix+4),h
	ld (ix+5),l
	push ix
	pop hl
	inc hl
	inc hl
	ld bc,6
	ldir
	ld (ix+0),30 ; destroyed saucer delay
	ld ix,SPR_SAUCER
	push ix
	pop hl
	inc hl
	inc hl
	ld bc,6
	ldir
	xor a
	ld (ix+0),a
; we need to either change the saucer sound and kill it in the statics for the score
; or kill it here...
	ld a,(sound_flag)
	res 3,a
	ld (sound_flag),a


	ex de,hl
	ld (flush_pointer),hl

; some kind of random saucer score here...

	call saucer_time
	pop ix

	ret

DESTROY_build_bomb 
	ld (destroy_what),ix
	ld hl,(flush_pointer)
	ex de,hl
	ld ix,(destroy_what)
	push ix
	pop hl
	inc hl
	inc hl
	ld bc,6
	ldir
	ld l,(ix+4)
	ld a,(ix+5)
	ld h,a	
	ld ix,SPR_BDAMAGE
	ld (ix+4),l
	ld (ix+5),h
	push ix
	pop hl
	inc hl
	inc hl
	ld bc,6
	ldir
	ex de,hl
	ld (flush_pointer),hl
	ld ix,(destroy_what)
	xor a
	ld (ix+4),a
	ld (ix+5),a	;delete bomb
	pop ix
	ret

destroy_missile_and_bomb

	ld (destroy_what),ix
	ld hl,(flush_pointer)
	ex de,hl
	ld ix,(destroy_what)
	push ix
	pop hl
	inc hl
	inc hl
	ld bc,6
	ldir   ; destroys the missile
	ld ix,SPR_PBULLET
	push ix
	pop hl
	inc hl
	inc hl
	ld bc,6
	ldir
; another kill bullet 
	ld a,(sound_flag)
	res 1,a
	ld (sound_flag),a
	xor a
	ld (ix+4),a
	ld (ix+5),a
	ex de,hl
	ld (flush_pointer),hl
	ld ix,(destroy_what)
	xor a
	ld (ix+4),a
	ld (ix+5),a	;delete bomb
	pop ix
	ret


destroy_build
	ld (destroy_what),ix
	ld hl,(flush_pointer)
	ex de,hl
	ld ix,(destroy_what)
	push ix
	pop hl
	inc hl
	inc hl
	ld bc,6
	ldir
	ld l,(ix+4)
	ld a,(ix+5)
	sub 4
	ld h,a	
	ld ix,SPR_BDAMAGE
	ld (ix+4),l
	ld (ix+5),h
	push ix
	pop hl
	inc hl
	inc hl
	ld bc,6
	ldir
	ex de,hl
	ld (flush_pointer),hl
	ld ix,(destroy_what)
	xor a
	ld (ix+4),a
	ld (ix+5),a	;delete bullet
; another kill bullet 
	ld a,(sound_flag)
	res 1,a
	ld (sound_flag),a

	pop ix
	ret

KILL_FLEET_SHIP ; 55-B = the entry of sprite collision to be removed
 	
	ld a,55 
	sub b
	ld l,a
	ld h,0
	LD DE,ALIEN_FLEET
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,DE ; hl points to the fleet entry hl+4 = fleet x , hl+5 = fleet y
	push hl
	pop ix		; sets up the static explosion part.
	ld a,(ix+4) ; fleetx
	ld c,(ix+5) ; fleety
	push ix
	ld ix,SPR_EXPLODE
	push af
	ld a,(ix+0)
	cp 0
	call nz,Static_Used
	pop af
	ld (ix+4),a
	ld (ix+5),c
	ld (ix+0),statics_duration  ; display explode time
	pop ix
	ld (hl),$00 ; delete this one from use - kill the ship!
	ld a,b
	inc hl
	inc hl
	ex de,hl
	ld hl,(flush_pointer)
	ex de,hl
	ld bc,6
	ldir		;overright the graphic
	ld ix,SPR_PBULLET
	push af
	ld a,$1f
	and (ix+7)
	ld (ix+7),a
	pop af
	push ix
	pop hl
	inc hl
	inc hl
	ld bc,6
	ldir
	push ix
; code for explosion.
	ld ix,SPR_Explode
	push ix
	pop hl
	inc hl
	inc hl
	ld bc,6
	LDIR
	pop ix
	ex de,hl
	ld (flush_pointer),hl
	ld b,a		

; another kill bullet 
	ld a,(sound_flag)
	res 1,a
	ld (sound_flag),a

	xor a
	ld (ix+4),a
	ld (ix+5),a	;delete bullet
	ld (credit+1),a
	ld (hundred+1),a
	ld a,b
	ld bc,0
	cp 12
	jr nc,lower2rows
	ld c,$10
lower2rows cp 34
	jr nc,mid2rows
	ld b,$10
mid2rows
	ld a,$10
	add a,b
	daa
	add a,c
	daa
	ld (digit+1),a
	ld hl,BCDS1+2
	call bcdadd
	call updates
	ld hl,NUMALIENS
	dec (hl)
	ld hl,sound_flag ; SET ALIEN HIT SOUND FLAG.
	set 0,(hl)
	pop ix
	ret
;SHIP8x8
COL8x8
	ld a,6
	ld (xok+1),a
	inc a
	ld (yok+1),a
	jp FLEET_COL

SAUCER_COL_DET
	ld a,16
	ld (xok+1),a
	ld a,7
	ld (yok+1),a
	jp FLEET_COL


ALLBUILDING
	ld a,12
	ld (xok+1),a
	ld a,10
	ld (yok+1),a
	jp FLEET_COL

ALLBUILDING_bomb
	ld a,15
	ld (xok+1),a
	ld a,12
	ld (yok+1),a
	jp FLEET_COL

missile_bomb
	ld a,3
	ld (xok+1),a
	ld a,4
	ld (yok+1),a
	jp FLEET_COL

;FLEET_COL 
;Enter 	DE - (X,Y) coords of First sprite
;	HL - (X,Y) coords of Second sprite
;Exits  Carry flag set if sprits collide.

FLEET_COL  LD A,D
	   SUB H
           jp p,xok
	   neg
xok        cp 6
	   ret nc ; outside the x dimension
	   LD A,E
	   SUB L
           jp p,yok
	   neg
yok        cp 7
	   ret nc ; outside the x dimension	
	   ret


;// ALIEN FIRING ROUTINES

FIRE_PREP ; find which ships if any are active to fire 
	  ; on each row. Anything less than 5 
	  ; is good to fire.
	push de
	push ix
	ld hl,active_fire
	ld de,active_fire+1
	ld bc,10
	ld (hl),0
	ldir
	ld bc,$050b
	ld ix,active_fire
findcol  push bc
findrow	 push bc
	 ld a,11
	 sub c
	 ld c,a
	 ld a,5
	 sub b
	 ld b,a
	 call ALIEN_SELECT
	 LD DE,ALIEN_FLEET
	 add hl,hl ; x2
         add hl,hl ; x4
	 add hl,hl ; x8
	 add hl,de
	 ld a,(hl)
	 cp 0
	 pop bc
	 jr nz,this_one
	 inc (ix+0)
	 djnz findrow
this_one pop bc
	 inc ix
	 dec c
	 jr nz,findcol
	 pop ix
	 pop de
	 ret

active_fire
	ds 11

ALIEN_SELECT
	; ENTER B = ROW 0 IS BOTTOM
	; C = COLUMN 0 IS LEFT
	; de is corrupted
	LD HL,0		; HARD CODED X 11
	LD D,0
	LD E,B
	ADD HL,DE
	SLA E
	RL D
	ADD HL,DE
	SLA E
	RL D
	SLA E
	RL D
	ADD HL,DE

	LD B,0
	ADD HL,BC	; OFFSET C

	RET

num_bombs equ 8 ; 8 bombs to test with

BOMB_ARRAY
	  ds 100

FILL_BOMBS   ld a,num_bombs
	     ld de,BOMB_ARRAY
BOMBARRAY_LOOP ld hl,SPR_BOMB
		ld bc,8
		ldir
		dec a
	        and a
		jr nz,bombarray_loop
	      ret


BOMBS_DROP  	LD IX,BOMB_ARRAY
	    	ld b,num_bombs
next_bomb   	push bc
		ld a,(ix+4)
		or (ix+5)
		jp z,NO_BOMB
		push ix
		LD A,$1F
		AND (IX+6)
		LD (IX+6),A
		pop hl
		inc hl
		inc hl
		ld BC,06
		LDIR
		ld a,(ix+5)
		inc a
		inc a
		cp 179
		jr nc,KILLALIENBOMB

		ld (ix+5),a
		ld a,3
		rrca
		rrca
		rrca
		or (ix+6)
		ld (ix+6),a
		ld hl,gaSHOT1
		ld a,(ix+0)
		and a
		jr z,bomb_anim
		INC (ix+5) ; MAKE PLUNGER SHOT GO A WEE BIT FASTER
		ld bc,16
		add hl,bc
bomb_anim
		ld a,(csecs)
		and 1
		jr z,bomb_even
		ld bc,08
		add hl,bc

bomb_even	
bombselected
		ld (ix+2),l
		ld (ix+3),h
		push ix
		pop hl
		inc hl
		inc hl
		ld BC,06
		LDIR
NO_BOMB		ld bc,08
		add ix,bc
		pop bc
		djnz NEXT_BOMB
		ret

KILLALIENBOMB 	xor a
		ld (ix+4),a
  		ld (ix+5),a
; some kind of alien bomb static blow up code needed here!

		jp NO_BOMB

;--------------
; Random Numbers
; arghhh
;--------------

SEED db 10

;--------------
; Random Numbers
; RANDOM
; RND (0-255)
;--------------

RANDOM LD a,(FRAMES)
	ld b,a
	ld a,(SEED)
	xor b
	ld b,a
	rrca
	rrca
	rrca
	xor $1f
	add a,b
	sbc a,$ff
	ld (seed) ,a
	ret
;--------------
; Random Numbers
; RANDOM11
; RND (0-11)
;--------------


RANDOM11 push de
	 call random
	 and a
	 ld h,0
	 ld l,a
	 ld d,12
	 xor a
	 ld b,$10
divloop  add hl,hl
	 rla
	 cp d
         jr c,divnext
	 sub d
	 inc l
divnext  djnz divloop
	 pop de
	 ret

firepattern dw $8000
firedirection db 0 ; 1 right 0; left
firelock	db 0

; based on the firepatter shoots bombs down from 
; a random alien in the fleet

; exits bc = FFFF no fire
; exits bc = b row c column to fire

FIRE	ld a,(firelock)
	ld b,a
	ld a,(numaliens)
	cp b
	jr firesequenceok

	push af
	cp 55
	call z,fpattern1
	pop af
	push af
	cp 40
	call z,fpattern2
	pop af
	cp 20
	call z,fpattern1

firesequenceok

	LD hl,firepattern
	rl (hl)
	inc hl
	rl (hl)
	jp c, LOCKNLOAD
	ld BC,$ffff
	ret

fpattern1 ld (firelock),a
	  push ix
	  ld ix,firepattern
	  ld (ix+0),0
	  ld (ix+1),$40
	  pop ix
	  ret

fpattern2 ld (firelock),a
	  push ix
	  ld ix,firepattern
	  ld (ix+0),$20
	  ld (ix+1),$40
	  pop ix
	  ret

LOCKNLOAD  dec hl
	   set 0,(hl)
	   ld a,(firedirection)
	   xor 1
	   ld (firedirection),a ; swap the ways through the things
	   call random11
	   and a
	   jr z, missfire  ; no fire...
	   dec a  ; a contains the column to shoot from.
	   ld h,10
retryfire   ld b,0
	   ld c,a
	   ld ix,active_fire
	   add ix,bc
	   ld b,(ix+0)
	   ld a,5
	   cp b
	   ret nz ; b column , c row

;this column isnt any good use nextone
	  dec h
	  jr z,missfire ; can'tfire no aliens founf
	  ld a,(firedirection)
	  cp 0
          jr dirleft
	  ld a,c
	  cp 10
	  jr c,inranger
 	  ld a,$ff
inranger  inc a  
	  jr retryfire
missfire   ld bc,$FFFF
	   ret

dirleft
	  ld a,c
	  cp 0
	  jr nz,inrangel
	  ld a,10
inrangel  dec a
	 jr retryfire




; enter 
; exit carry clear bomb found
; carry set no ammo
Freebomb
	 ld IX,BOMB_ARRAY
	 ld b,num_bombs
findbomb ld a,(ix+4)
	 or (ix+5)
	 jr nz,bombused
; this bomb is free its the one we need
	scf
	ret	 
bombused push bc
	ld bc,08
	add ix,bc
	pop bc
	djnz findbomb
	scf
	ccf
; got no ammo set carry flag 
	ret

ALIEN_FIRE_CONTROL
	
	  call fire
	  ld a,b
	  and c
	  cp $FF
	  ret z ; do nothing

testfire  nop
	  nop
	 PUSH DE

         push bc
         pop hl ; temp store bc in hl

; find free bullet in array

	call Freebomb
	jr nc,no_ammo ; no free bombs

; here IX points to free bomb slot
;  b is the row and c is the column
; find alien in fleet to shoot from

	push hl
	pop bc ; get bc back
	 call ALIEN_SELECT
	 LD DE,ALIEN_FLEET
	 add hl,hl ; x2
         add hl,hl ; x4
	 add hl,hl ; x8
	 add hl,de

; IX points to the free bomb
;HL is now the alien in the fleet we need

	PUSH IX
	PUSH HL
	POP IX
	ld d,(ix+4)
	ld a,(ix+5)
	POP IX
	ld (ix+4),d
	add a,8
	ld (ix+5),a
	ld hl,gaSHOT1
	ld a,r ; bullet choice
	and 1
	and a
	jr z,shotselected
	ld bc,16
	add hl,bc
shotselected
	ld (ix+0),a ; type of bomb
	ld (ix+2),l
	ld (ix+3),h
	
; bullet is set up we need to display it
	PUSH IX
	LD A,$1F
	AND (IX+6)
	LD (IX+6),A
	POP HL
	INC HL
	INC HL
	POP DE
	LD BC,6
	LDIR
	RET

; add it into array...
no_ammo	POP DE
usethisone ret

sprXOR_cd_bombs	
	push bc
	push bc
	call fbuff
	call fixbuff	
	CALL PLOT ; DE NOW SCREEN ADDRESS
	DEC DE
	pop bc
	ld b,3
	ld hl,gBUFF
	call g_XOR_cd
	ld a,c
	and $FF
	call nz,BOMB_COLLISION ; BOMB_COLLISION
	pop bc
	RET

BOMB_COLLISION
	
; on entry here IX points to the gbuffer  and will require saved
	push ix

; find what bomb that has hit
	
	ld h,(ix+2) ; bomb x
	ld l,(ix+3) ; bomb y
	ld ix,BOMB_ARRAY   ; ix+4  bomb x , ix+5 bomb y
	ld b,num_bombs
bomb_check_loop 
	push bc
	ld a,(ix+4)
	cp h
	jr nz,check_next_bomb
	ld a,(ix+5)
	cp l
	jr z,this_is_the_bomb
check_next_bomb
	ld bc,08
	add ix,bc
	pop bc	
	djnz bomb_check_loop
; if we get here no striking bomb found so ignore.
	pop ix
	ret

this_is_the_bomb ; ix now points to the bomb - original ix still on the stack

; find the collission
	pop bc ; clear the stack

; is it the missile?

	ld a,(ix+4) ; bomb already mid position
	ld h,a
	ld a,(ix+5) ; bomb 2 below mid
	add a,2
	ld l,a ; now the bombs (x,y) to check

	push ix
	ld ix,SPR_PBULLET

	ld a,(ix+4) ; bomb already mid position
	dec a
	ld d,a
	ld a,(ix+5) ; bomb 2 below mid
	add a,2
	ld e,a ; now the bombs (x,y) to check
	pop ix

	call missile_bomb
	jp c,destroy_missile_and_bomb


; is it a building
BOMB_BUILD_COL
	ld a,(ix+4) ; bomb already mid position
        dec a
	ld h,a
	ld a,(ix+5) ; bomb 2 below mid
	add a,2
	ld l,a ; now the bombs (x,y) to check
	ld d,44
	ld e,152; (d,e) hold mid of 1st building
	call ALLBUILDING_bomb
	jp c,destroy_build_bomb
	ld d,100
	call ALLBUILDING_bomb
	jp c,destroy_build_bomb
	ld d,156
	call ALLBUILDING_bomb
	jp c,destroy_build_bomb
	ld d,212
	call ALLBUILDING_bomb
	jp c,destroy_build_bomb

;okay its not them is it the player?

	ld a,(ix+4) ; bomb already mid position
        dec a
	ld h,a
	ld a,(ix+5) ; bomb 2 below mid
	add a,2
	ld l,a ; now the bombs (x,y) to check

	push ix
	ld ix,SPR_PLY

	ld a,(ix+4) ; bomb already mid position
	dec a
	ld d,a
	ld a,(ix+5) ; bomb 2 below mid
	add a,2
	ld e,a ; now the bombs (x,y) to check
	pop ix

	call SAUCER_COL_DET  ; same detection size for player...

	jr c,hit_player_detected

	pop ix
	RET

hit_player_detected
	ld hl,keyflag
	set 6,(hl)
	pop ix
	ret

; Saucer code

SAUCER_MOVE  
	LD IX,SPR_SAUCER
	LD A,(IX+0)
	CP 0
	RET Z ; NO SAUCER TO MOVE
	PUSH IX
	POP HL
	INC HL
	INC HL
	LD BC,06
	LDIR
	LD A,(IX+4)
SAUCER_DIR
	dec A ; 3c is inc a 
	CP 247
	JP NC,SAUCER_DONE
	CP 9
	JP C,SAUCER_DONE
	LD (IX+4),A
saucer_show
	PUSH IX
	POP HL
	INC HL
	INC HL
	LD BC,6
	LDIR

; initialise saucer sound flag here...
	ld a,(sound_flag)
	set 3,a
	ld (sound_flag),a

	RET

SAUCER_DONE
	LD (IX+0),0
; cancel saucer sound here
	ld a,(sound_flag)
	res 3,a
	ld (sound_flag),a

	call saucer_time ; reset for next saucer
	RET

Saucer_Delay dw 0100; 3000 ; delayed 1 min before it'll do anything

saucer_time

	push de
	ld ix,saucer_delay
	call random11
	cp 8
	jr nc,random_sauc_ok
	ld a,8
random_sauc_ok
	ld (ix+1),a
	ld a,(numaliens)
	ld (ix+0),a
	pop de
	ret

SAUCER_INIT
	LD IX,Saucer_Delay
	LD H,(IX+1)
	ld L,(ix+0)
	ld a,h
	or l
	jr z,saucer_chance
	dec hl
	ld (ix+1),H
	ld (ix+0),l
	ret
saucer_chance
	LD IX,SPR_SAUCER
	LD A,(IX+0)
	CP 0
	RET NZ ; ALREADY A SAUCER

; set sound for saucer start and cancel any fleet sound happening
	call saucers_snd
	xor a
	ld (fleet_snd_reset),a

	CALL RANDOM
	CP 128
	ret C ; NO_SAUCER this time
	SUB 128
	CP 69
	JR C,L2R
R2L	LD HL,$10F7 ; RIGHT CORNER
	LD(IX+4),L
	LD (IX+5),H
	LD HL,SAUCER_DIR
	LD (HL),$3d ; OP CODE DEC A
	LD (IX+0),1
	JP saucer_show	
L2R	LD HL,$1008 ; RIGHT CORNER
	LD(IX+5),h
	LD (IX+4),L
	LD HL,SAUCER_DIR
	LD (HL),$3c ; OP CODE INC A
	LD (IX+0),2
 	jp saucer_show	

;----------
;UPD_HISCORE
; Updates the hiscore at end of game
;----------
UPD_HISCORE LD IX,BCDS1
	    LD A,(IX+0)
	    CP (IX+6)
	    ret c
	    jr z,hi2
	    jr nc,newhi
hi2	    LD A,(IX+1)
	    CP (IX+7)
	    ret c
	    jr z,hi3
	    jr nc,newhi	
hi3	    LD A,(IX+2)
	    CP (IX+8)
	    ret c
	    ret z ; samehi
newhi	    PUSH IX
	    POP DE
	    LD HL,06
	    ADD HL,DE
	    EX DE,HL
	    LD BC,3
            LDIR
	    LD A,7
	    LD (SHOW_FLAG),A
	    CALL UPDATES
	    RET

;SOUNDS FLAGS - 
; BIT0 = FLEET HIT  ( ZX Beeps ) 
; BIT1 = MISSILE HISS ( AY Channel B+ Noise )
; BIT2 = PLAYER HIT
; BIT3= SAUCER( AY - Channel C )
; BIT4 = FLEET MOVES  (AY - Channel A )

sound_Flag db 0

ay_flag   db 0

wave_form db 0

sound_a_time db 0

sound_b_time db 0

all_sounds ld a,$7f ; all sounds off
	   ld (ay_flag),a
	   
	   ld a,(sound_flag)
	   bit 0,a
	   call nz,ahit_sound

	   ld a,(sound_flag)
	   bit 3,a
	   jr z,no_chanc
	   ld a,(ay_flag)
	  and $7b ; c channel to make tone 
	  ld (ay_flag),a
no_chanc
	   bit 4,a
	   call nz,fleet_sound

	   ld a,(fleet_snd_reset)
	   and a
	   jr z,no_chana

	   dec a
	  ld (fleet_snd_reset),a

	  ld a,(ay_flag)
	  and $7e ; a channel to make tone
	  ld (ay_flag),a

no_chana
	
	   ld a,(sound_flag)
	   bit 1,a
	   jr z, no_chanb

	  ld a,(ay_flag)
	  and $6d ; b channel to make noise+tone
	  ld (ay_flag),a


no_chanb


ay_skip:

	   ld a,(sound_flag)
	   bit 3,a
	   jr nz,no_oscillator ; turns c volume off

	 ld d,10
	 ld e,0
	call ay_regpoke

no_oscillator	
	   call ay_set
	   ret

ay_chanaoff:

	 ld d,7
	 ld e,$7f
ay_regpoke:
 	ld bc,65533
	out (c),d
	ld bc,49149
	out (c),e
	ret

ay_set:

	 ld d,7
	ld a,(ay_flag)
	 ld e,a
 	ld bc,65533
	out (c),d
	ld bc,49149
	out (c),e
	ret


FLEET_SND DB 0

FLEET_SND_RESET DB 0 ; time to loop before killing it

; FLEET MOVE SOUND.
Fleet_SOUND   PUSH DE
	   LD A,(FLEET_SND)
	   add a,a ;x2
	   add a,a
	   add a,a ;x8
	   add a,a ;x16 
	   ld e,a  
           ld d,0
	   ld hl,fleet_snd1
	   add hl,de
	   push hl
	   pop ix
	   ld a,(FLEET_SND)
	   inc a
           and 03
	   ld (FLEET_SND),a
	   ld a,5
	   ld (FLEET_SND_RESET),a ; set the delay before turn off
	   ld b,3
	   call aysetreg
	   ld hl,sound_flag
	   res 4,(hl)

fa_exit:
	   POP DE
	   RET

aysetreg:
; enter hl data pairs ( 5 off regs 0,1,8,11,12 )
; enter b number of regs to out ot.
;	 ld b,3 ; pair 8 is the one to force this channel!!
ay_regloop:
	push bc
	ld d,(hl)
	inc hl
	ld e,(hl)
	inc hl
	ld bc,65533
	out (c),d
	ld bc,49149
	out (c),e
	pop bc
	djnz ay_regloop
	ret
	
missiles_snd push hl
	     push de
	     ld hl,missile_snd5
	     ld b,3
	     call aysetreg
	     pop de
	     pop hl
	     ret

saucers_snd push hl
	     push de
	     ld hl,saucer_snd6
	     ld b,7
	     call aysetreg
	     pop de
	     pop hl
	     ret

waveform

	ld d,10
	ld e,a
	ld bc,65533
	out (c),d
	ld bc,49149
	out (c),e
	 ret


test_saucer_snd
	  ld hl,saucer_snd6
	  call saucers_snd
	ld d,7
	ld e,$7b
	ld bc,65533
	out (c),d
	ld bc,49149
	out (c),e

	  ret


ahit_sound:
;rem alien hit sound - sound flag 0 cancels on completion of sound 
	ld hl,175*256+14
	call tone8
	ld hl,sound_flag
	res 0,(hl)
	RET

tone8	ld c,l
	xor a
here:	xor 16
	out ($fe),a
	ld b,h
there:	djnz there
	dec c
	jr nz,here
	ret

ay_sound_control:
; sound chip control routines

;ay_data
; sound chip register+data information
fleet_snd1:
	db 0,$00,1,7,8,7,11,40,12,0,13,0,6,2,7,$7e

fleet_snd2:
	db 0,$00,1,8,8,7,11,40,12,0,13,0,6,2,7,$7e

fleet_snd3:
	db 0,$00,1,9,8,7,11,40,12,0,13,0,6,2,7,$7e

fleet_snd4:
	db 0,$00,1,$0a,8,7,11,40,12,0,13,0,6,2,7,$7e

missile_snd5:
	db 2,$10,3,$0,9,11,11,40,12,0,13,0,6,$07,7,$6d

saucer_snd6:
	db 4,$20,$5,$1,10,16,11,40,12,1,13,14,6,2,7,$7b

saucer_snd7:
	db 4,$a0,5,$4,10,16,11,40,12,1,13,14,6,2,7,$7b

kill_snd:
	db 0,0,1,0,8,0,11,0,12,0,13,0,7,$7f,0,0

;GFLUSH
; Graphics to be updated next sweep.
; format is :
; dw sprite_pointer
; dw coordinates
; dw sprite_dimension - special codes here to be explained.
; 0000 means end of list
; list cleared at end. ( 256 spaces for data)
GFLUSH
	ds 256  ; 256 spaces for graphics buffer data

db "END"



